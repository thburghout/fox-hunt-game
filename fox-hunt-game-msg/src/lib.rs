use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

pub type TeamId = i64;
pub type Progress = f32;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Message {
    Clues(Vec<Clue>),
    ClueUpdate(Clue),
    TeamProgress(Team, Progress),
    Done,
}

pub type ClueId = i64;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Clue {
    pub id: ClueId,
    pub title: String,
    pub content: String,
    pub published_at: DateTime<Utc>,
    pub state: ClueState,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SolvedClueProps {
    pub location: LatLng,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct AvailableClueProps {
    pub hints: Vec<String>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum ClueState {
    Available(AvailableClueProps),
    Solved(SolvedClueProps),
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct LatLng {
    pub lat: f32,
    pub lng: f32,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct Team {
    pub id: TeamId,
    pub name: String,
    pub color: String,
}

impl Clue {
    pub fn new(
        id: i64,
        title: String,
        content: String,
        published_at: DateTime<Utc>,
        state: ClueState,
    ) -> Self {
        Clue {
            id,
            title,
            content,
            published_at,
            state,
        }
    }
}
