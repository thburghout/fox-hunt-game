FROM alpine:3.12
RUN mkdir /var/lib/fox-hunt-game && \
    mkdir /var/lib/fox-hunt-game/state && \
    mkdir /var/lib/fox-hunt-game/dist && \
    touch /var/lib/fox-hunt-game/state/storage.db
COPY ./target/release/* /usr/local/bin/
COPY ./fox-hunt-game-ui/dist /var/lib/fox-hunt-game/dist

EXPOSE 80
ENTRYPOINT [ "serve" ]
