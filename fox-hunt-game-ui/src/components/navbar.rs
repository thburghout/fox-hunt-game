use super::button::Button;

use yew::format::Nothing;
use yew::prelude::*;
use yew::services::fetch::{FetchTask, Request, Response};

use yew::services::FetchService;
use yew::Callback;

pub struct Nav {
    props: NavProps,
    link: ComponentLink<Self>,
    logout_task: Option<FetchTask>,
}

#[derive(Properties, Clone, PartialEq)]
pub struct NavProps {
    pub on_logout: Callback<()>,

    pub children: Children,
}

pub enum Msg {
    LogOut,
    LogOutFinished,
}

impl Component for Nav {
    type Message = Msg;
    type Properties = NavProps;
    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            props,
            link,
            logout_task: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::LogOut => {
                let request = Request::post("/api/v1/logout")
                    .body(Nothing)
                    .expect("Could not build request.");
                let callback = self
                    .link
                    .callback(|_response: Response<Nothing>| Msg::LogOutFinished);
                self.logout_task = FetchService::fetch(request, callback)
                    .expect("Failed to start request")
                    .into();
                true
            }
            Msg::LogOutFinished => {
                self.props.on_logout.emit(());
                false
            }
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if props != self.props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        html!(
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <span class="navbar-brand">{"Hunt"}</span>
                        { for self.props.children.iter() }
                    <Button class="outline-info" onclick=self.link.callback(|_| Msg::LogOut )
                        is_loading=self.logout_task.is_some()>
                        {"Log out"}
                    </Button>
                </div>
            </nav>
        )
    }
}
