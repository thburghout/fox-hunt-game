use crate::components::markdown::Markdown;
use chrono::{DateTime, Local};
use yew::prelude::*;

pub(crate) struct Clue {
    props: ClueProps,
    link: ComponentLink<Self>,
}

#[derive(Properties, Clone, PartialEq)]
pub struct ClueProps {
    pub title: String,
    pub content: String,
    pub status: ClueStatus,
    pub published_at: DateTime<Local>,
    pub hints: Vec<String>,
}

#[derive(Copy, Clone, PartialEq)]
pub enum ClueStatus {
    Available,
    Solved,
}

impl Component for Clue {
    type Message = ();
    type Properties = ClueProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self { props, link }
    }

    fn update(&mut self, _msg: Self::Message) -> bool {
        true
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        if props != self.props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        html! {
            <div class="card mt-2">
                <div class="card-body">
                    <h5 class="card-title">
                        <i class=self.get_status_class()></i>
                        {" "}
                        {self.props.title.clone()}
                    </h5>
                    <h6 class="card-subtitle text-muted mb-2">
                        { self.props.published_at.format("%H:%M") }
                    </h6>
                    { for self.props.hints.iter().map(|x| html!(<Hint hint=x.clone() />)) }
                    <p class="card-text">
                        <Markdown>{ self.props.content.clone() }</Markdown>
                    </p>
                </div>
            </div>
        }
    }
}

impl Clue {
    fn get_status_class(&self) -> String {
        "bi bi-patch-".to_string()
            + match self.props.status {
                ClueStatus::Available => "question",
                ClueStatus::Solved => "check-fill",
            }
    }
}

struct Hint {
    link: ComponentLink<Self>,
    hint: String,
    shown: bool,
}

#[derive(Properties, Clone, PartialEq)]
struct HintProps {
    hint: String,
}

impl Component for Hint {
    type Message = bool;
    type Properties = HintProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            hint: props.hint,
            shown: false,
        }
    }

    fn update(&mut self, x: Self::Message) -> bool {
        self.shown = x;
        true
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        if self.hint != props.hint {
            self.hint = props.hint;
            self.shown = false;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        if self.shown {
            html! {
                <p class="card-text">
                    <Markdown>{ format!("**Hint:** {}", self.hint) }</Markdown>
                </p>
            }
        } else {
            html! {
                <p class="card-text">
                    <span class="fw-bold">{"Hint: "}</span>
                    <a href="#" onclick=self.link.callback(|_| true)>{"show..."}</a>
                </p>
            }
        }
    }
}
