use crate::agents::modal_service::{Modal, ModalService};
use yew::prelude::*;

pub struct ModalProvider {
    props: (),
    link: ComponentLink<Self>,
    modal: Option<Modal>,
    service: Box<dyn Bridge<ModalService>>,
}

pub enum Msg {
    Show(Modal),
    Close,
}

impl Component for ModalProvider {
    type Message = Msg;
    type Properties = ();

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let service = ModalService::bridge(link.callback(Msg::Show));
        Self {
            props,
            link,
            modal: None,
            service,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Show(msg) => {
                self.modal = Some(msg);
            }
            Msg::Close => {
                self.modal = None;
            }
        }
        true
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        html!(
            <div class={if self.modal.is_some() {"modal fade show"} else {"modal fade"}}
                 tabindex="-1"
                 style={if self.modal.is_some() {"display: block;"} else {"display: none;"}}>
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">{self.title()}</h5>
                        </div>
                        <div class="modal-body">
                            <p>{self.message()}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary"
                                onclick=self.link.callback(|_| { Msg::Close }) >
                                {"Ok"}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

impl ModalProvider {
    fn title(&self) -> String {
        self.modal
            .as_ref()
            .map_or(String::new(), |m| m.title.clone())
    }

    fn message(&self) -> String {
        self.modal
            .as_ref()
            .map_or(String::new(), |m| m.message.clone())
    }
}
