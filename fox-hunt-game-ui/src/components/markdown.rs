use yew::html::ChildrenRenderer;
use yew::prelude::*;

pub struct Markdown(Props);

#[derive(Properties, Clone, PartialEq)]
pub struct Props {
    pub children: ChildrenRenderer<String>,

    #[prop_or_default]
    pub class: String,
}

impl Component for Markdown {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self { 0: props }
    }

    fn update(&mut self, _: Self::Message) -> bool {
        false
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        if props != self.0 {
            self.0 = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        let text = &self.0.children.iter().next().unwrap_or_default();
        let parser = pulldown_cmark::Parser::new(text);
        let mut html_output: String = String::with_capacity(text.len() * 3 / 2);
        pulldown_cmark::html::push_html(&mut html_output, parser);
        let p = yew::utils::document().create_element("span").unwrap();
        p.set_inner_html(&html_output);
        p.set_class_name(&self.0.class);
        Html::VRef(p.into())
    }
}
