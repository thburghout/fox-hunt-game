use crate::components::dashboard::Dashboard;
use crate::components::login_form::LoginForm;

use yew::prelude::{html, Component, ComponentLink, Html, ShouldRender};

pub struct App {
    _props: (),
    link: ComponentLink<Self>,
    logged_in: bool,
    message: Option<String>,
}

pub enum Msg {
    UserLoggedIn,
    UserLoggedOut(Option<String>),
}

impl Component for App {
    type Message = Msg;
    type Properties = ();
    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            _props: props,
            link,
            logged_in: true,
            message: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::UserLoggedIn => {
                self.logged_in = true;
            }
            Msg::UserLoggedOut(None) => {
                self.logged_in = false;
            }
            Msg::UserLoggedOut(Some(message)) => {
                self.message = message.into();
                self.logged_in = false;
            }
        }
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        if !self.logged_in {
            html! {<LoginForm on_login=self.link.callback(|_| {Msg::UserLoggedIn} )
            error=self.message.clone() />}
        } else {
            html! {<Dashboard on_logout=self.link.callback(|msg| {Msg::UserLoggedOut(msg) } )/>}
        }
    }
}
