use yew::prelude::*;

const TOTAL_HEIGHT: i32 = 40;

pub struct StatusBar {
    props: Props,
    link: ComponentLink<Self>,
}

#[derive(Debug, Properties, Clone, PartialEq)]
pub struct Props {
    pub primary: f32,
    pub color: String,
    pub secondaries: Vec<(String, f32)>,
}

impl Component for StatusBar {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self { props, link }
    }

    fn update(&mut self, _msg: Self::Message) -> bool {
        true
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        if self.props != props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        if self.props.secondaries.is_empty() {
            return html!();
        }
        let height = TOTAL_HEIGHT / 2 / self.props.secondaries.len() as i32;
        html!(
            <nav id="status-bar" class="navbar fixed-bottom navbar-dark bg-light p-0">
                <TeamProgress height=(TOTAL_HEIGHT / 2) progress=self.props.primary color=self.props.color.clone() />
                { for self.props.secondaries.iter().map(|x| StatusBar::view_secondary(x, height)) }
            </nav>
        )
    }
}

impl StatusBar {
    fn view_secondary((color, progress): &(String, f32), height: i32) -> Html {
        html!(
            <TeamProgress height=height progress=progress color=color.clone()/>
        )
    }
}

struct TeamProgress {
    props: ProgressProps,
    link: ComponentLink<Self>,
}

#[derive(Properties, Clone, PartialEq)]
pub struct ProgressProps {
    height: i32,
    progress: f32,
    color: String,
}

impl Component for TeamProgress {
    type Message = ();
    type Properties = ProgressProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self { props, link }
    }

    fn update(&mut self, _msg: Self::Message) -> bool {
        true
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        if self.props != props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        let progress = self.props.progress * 100.0;
        html!(
            <div class="progress"
                 style=format!("height: {}px; width: 100%;", self.props.height)>
                <div class="progress-bar" role="progressbar"
                     style=format!("width: {}%; background: {}; border-radius: 0;",
                                   progress, self.props.color)
                     aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                </div>
            </div>
        )
    }
}
