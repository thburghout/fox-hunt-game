use yew::prelude::*;
use yew::MouseEvent;

pub(crate) struct Button(Props, ComponentLink<Self>);

#[derive(Properties, Clone, PartialEq)]
pub struct Props {
    #[prop_or("primary".to_string())]
    pub class: String,
    #[prop_or(false)]
    pub is_loading: bool,
    #[prop_or(false)]
    pub disabled: bool,
    #[prop_or_default]
    pub onclick: Option<Callback<MouseEvent>>,
    pub children: Children,
}

pub enum Msg {
    Clicked(MouseEvent),
}

impl Component for Button {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self { 0: props, 1: link }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Clicked(e) => {
                if let Some(func) = self.0.onclick.as_ref() {
                    func.emit(e);
                }
            }
        }
        false
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        if self.0 != props {
            self.0 = props;
            return true;
        }
        false
    }

    fn view(&self) -> Html {
        html! {<button
            class={format!("btn btn-{}", self.0.class)}
            type="button"
            disabled=self.0.disabled
            onclick=self.1.callback(|x| { Msg::Clicked(x) })>
                <span class={ if self.0.is_loading {"spinner-grow spinner-grow-sm"}
                              else {""}} role="status" aria-hidden="true"></span>
                { self.0.children.clone() }
        </button>}
    }
}
