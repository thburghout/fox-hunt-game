use yew::prelude::*;

use super::navbar::Nav;
use anyhow::Error;
use http::StatusCode;
use leaflet::LatLng;
use yew::format::{Json, Nothing};
use yew::services::fetch::{FetchTask, Request, Response};
use yew::services::websocket::{WebSocketStatus, WebSocketTask};
use yew::services::{ConsoleService, FetchService, TimeoutService, WebSocketService};
use yew::Callback;

use fox_hunt_game_msg::{Clue as ClueModel, ClueId, ClueState, Message, Team, TeamId};

use super::clue::Clue;
use super::game_map::GameMap;
use super::status_bar::StatusBar;
use serde_json::Value;

use super::modal_provider::ModalProvider;
use crate::agents::modal_service::{Modal, ModalService};
use crate::components::clue::ClueStatus;
use chrono::Local;
use std::cmp::Reverse;
use std::collections::HashMap;
use std::time::Duration;
use web_sys::Location;
use yew::agent::Dispatcher;
use yew::services::timeout::TimeoutTask;

pub struct Dashboard {
    props: DashboardProps,
    link: ComponentLink<Self>,
    ws: Option<WebSocketTask>,
    clues: Vec<ClueModel>,
    current_clue: Option<ClueId>,
    fetch_task: Option<FetchTask>,
    service: Dispatcher<ModalService>,
    progresses: HashMap<TeamId, (Team, f32)>,
    me: Option<Team>,
    done: bool,
    state: State,
    timer: Option<TimeoutTask>,
}

#[derive(Properties, Clone, PartialEq)]
pub struct DashboardProps {
    pub on_logout: Callback<Option<String>>,
}

#[derive(Debug)]
pub enum Msg {
    Received(Result<Message, Error>),
    Status(WebSocketStatus),
    Error(Error),
    SubmitSolution(LatLng),
    SolutionCorrect(bool),
    DisplayError(String),
    SetMe(Team),
    Unauthorized,
    Logout,
    ConnectWebsocket,
}

#[derive(PartialEq)]
enum State {
    Idle,
    WaitForResponse,
}

impl Component for Dashboard {
    type Message = Msg;
    type Properties = DashboardProps;
    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let mut dashboard = Dashboard {
            props,
            link,
            ws: None,
            clues: vec![],
            current_clue: None,
            fetch_task: None,
            service: ModalService::dispatcher(),
            progresses: Default::default(),
            me: None,
            done: false,
            state: State::Idle,
            timer: None,
        };
        dashboard.start_fetch_me_task();
        dashboard.link.send_message(Msg::ConnectWebsocket);
        dashboard
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Received(Ok(Message::Clues(clues))) => {
                self.clues = clues;
                self.handle_clues_changed();
                self.check_done();
                self.state = State::Idle;
                true
            }
            Msg::Received(Ok(Message::ClueUpdate(clue))) => {
                if let Some(existing) = self.clues.iter_mut().find(|c| c.id == clue.id) {
                    *existing = clue;
                } else {
                    self.clues.push(clue);
                }
                self.handle_clues_changed();
                true
            }
            Msg::Received(Ok(Message::Done)) => {
                self.done = true;
                true
            }
            Msg::SetMe(team) => {
                self.me = Some(team);
                true
            }
            Msg::Received(Ok(Message::TeamProgress(team, progress))) => {
                if let Some(me) = &self.me {
                    if me.id == team.id && (progress - 1.0) > -0.05 {
                        self.done = true;
                    }
                }
                self.progresses.insert(team.id, (team, progress));
                true
            }
            Msg::SubmitSolution(pos) => {
                if let Some(clue_id) = self.current_clue {
                    self.submit_solution(clue_id, pos);
                    self.state = State::WaitForResponse;
                    true
                } else {
                    self.service
                        .send(Modal::title("Error").message("No clue to solve."));
                    false
                }
            }
            Msg::DisplayError(error) => {
                self.service.send(Modal::title("Error").message(&error));
                self.state = State::Idle;
                true
            }
            Msg::SolutionCorrect(correct) => {
                self.state = State::Idle;
                let modal = Modal::title("Clue solution").message(if correct {
                    "Correct!"
                } else {
                    "Wrong"
                });
                self.service.send(modal);
                true
            }
            Msg::Unauthorized => {
                self.props.on_logout.emit(if self.me.is_some() {
                    Some("An error occurred, please log in again.".to_owned())
                } else {
                    None
                });
                false
            }
            Msg::Logout => {
                self.props.on_logout.emit(None);
                false
            }
            Msg::Status(s) => {
                match s {
                    WebSocketStatus::Closed | WebSocketStatus::Error => {
                        self.timer = Some(TimeoutService::spawn(
                            Duration::from_secs(3),
                            self.link.callback(|_| Msg::ConnectWebsocket),
                        ))
                    }
                    WebSocketStatus::Opened => {}
                }
                true
            }
            Msg::ConnectWebsocket => {
                self.timer = None;
                let callback = self.link.callback(|Json(data)| Msg::Received(data));
                let notification = self.link.callback(Msg::Status);
                self.ws = Some(
                    WebSocketService::connect(
                        &Dashboard::get_websocket_url().unwrap(),
                        callback,
                        notification,
                    )
                    .unwrap(),
                );
                false
            }
            x => {
                ConsoleService::log(&format!("{:?}", x));
                false
            }
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        let path: Vec<LatLng> = self
            .clues
            .iter()
            .filter_map(|c| match &c.state {
                ClueState::Available(_) => None,
                ClueState::Solved(x) => {
                    Some(LatLng::new(x.location.lat as f64, x.location.lng as f64))
                }
            })
            .collect();
        let ws_status = self
            .timer
            .as_ref()
            .map(|_| "Lost connection, reconnecting...");
        let nav_body = match (ws_status, &self.me) {
            (Some(status), _) => html!(<span class="badge bg-danger">{status}</span>),
            (_, Some(team)) => html!(<>
                <div class="color-box" style=format!("background: {};", team.color)></div>
                <span class="navbar-text">{&team.name}</span></>),
            (_, _) => html!(),
        };
        html! {<>
            <ModalProvider/>
            <Nav on_logout=self.link.callback(|_| {Msg::Logout}) >
                <span>{nav_body}</span>
            </Nav>
            <GameMap disabled={ self.state == State::WaitForResponse || self.current_clue.is_none() }
                     loading={self.state == State::WaitForResponse}
                     on_submit=self.link.callback(|x| { Msg::SubmitSolution(x) })
                     path=path />
            <main class="container clues">
                { self.view_finished() }
                { for self.clues.iter().map(|c| Dashboard::view_clue(c)) }
            </main>
            { self.view_status_bar() }
        </>}
    }
}

impl Dashboard {
    fn view_clue(clue: &ClueModel) -> Html {
        let publish_time = clue.published_at.with_timezone(&Local);
        let status: ClueStatus = clue.state.clone().into();
        let hints = match &clue.state {
            ClueState::Available(x) => x.hints.clone(),
            _ => vec![],
        };
        html!(
            <Clue   title=clue.title.clone()
                    content=clue.content.clone()
                    status=status
                    published_at=publish_time
                    hints=hints
                    />
        )
    }

    fn view_status_bar(&self) -> Html {
        let my_progress = self
            .me
            .as_ref()
            .map(|x| {
                self.progresses
                    .get(&x.id)
                    .map(|(t, p)| (t.color.clone(), p))
            })
            .flatten();
        let secondaries = match &self.me {
            None => {
                vec![]
            }
            Some(x) => self
                .progresses
                .iter()
                .filter_map(|(k, (t, p))| {
                    if *k == x.id {
                        None
                    } else {
                        Some((t.color.clone(), *p))
                    }
                })
                .collect(),
        };
        if let Some((my_color, my_progress)) = my_progress {
            html!(<StatusBar primary=my_progress color=my_color secondaries=secondaries/>)
        } else {
            html!()
        }
    }

    fn view_finished(&self) -> Html {
        if self.done {
            html!(
                <div class="card mt-2">
                    <div class="card-body">
                        <h5 class="card-title">{"The End"}</h5>
                        <p class="card-text">{ "Congratulations, you solved all the clues. \
                            You can close this page now. Thanks for playing!" }</p>
                    </div>
                    <img src="https://media3.giphy.com/media/11sBLVxNs7v6WA/giphy.gif?cid=ecf05e47koe02qg9bcxhjq1wqwvb4tuqixin6xbvny6xnt8u&rid=giphy.gif"
                        class="card-img-bottom"/>
                </div>
            )
        } else {
            html!()
        }
    }

    fn submit_solution(&mut self, id: ClueId, pos: LatLng) {
        let request = Request::post(format!("/api/v1/clue/{}/solve", id))
            .header("Content-Type", "application/json")
            .body(Json(&pos))
            .expect("Error building request.");
        let callback =
            self.link
                .callback(|response: Response<Json<Result<Value, anyhow::Error>>>| {
                    if response.status() == StatusCode::UNAUTHORIZED {
                        return Msg::Unauthorized;
                    }
                    let Json(data) = response.into_body();
                    match data {
                        Ok(Value::Object(result)) => {
                            if let Some(Value::String(x)) = result.get("result") {
                                Msg::SolutionCorrect(x == "ok")
                            } else {
                                Msg::DisplayError("Error parsing response.".into())
                            }
                        }
                        _ => Msg::DisplayError("Error submitting solution.".into()),
                    }
                });
        self.fetch_task = FetchService::fetch(request, callback).unwrap().into();
    }

    fn start_fetch_me_task(&mut self) {
        let request = Request::get("/api/v1/me")
            .header("Content-Type", "application/json")
            .body(Nothing)
            .expect("Error building request.");
        let callback =
            self.link
                .callback(|response: Response<Json<Result<Team, anyhow::Error>>>| {
                    if response.status() == StatusCode::UNAUTHORIZED {
                        Msg::Unauthorized
                    } else {
                        let Json(team) = response.into_body();
                        Msg::SetMe(team.unwrap())
                    }
                });
        self.fetch_task = FetchService::fetch(request, callback).unwrap().into();
    }

    fn handle_clues_changed(&mut self) {
        self.clues.sort_by_key(|c| Reverse(c.published_at));
        self.current_clue = self
            .clues
            .iter()
            .find(|c| matches!(c.state, ClueState::Available(_)))
            .map(|c| c.id);
    }

    fn check_done(&mut self) {
        if !self.clues.is_empty() {
            self.done = self
                .clues
                .iter()
                .map(|c| matches!(c.state, ClueState::Solved(_)))
                .all(|y| y);
        }
    }

    fn get_websocket_url() -> Option<String> {
        let location: Location = web_sys::window()?.location();
        let protocol = if location.protocol().ok()? == "http:" {
            "ws"
        } else {
            "wss"
        };
        Some(format!("{}://{}/ws", protocol, location.host().ok()?))
    }
}

#[allow(clippy::from_over_into)]
impl Into<ClueStatus> for ClueState {
    fn into(self) -> ClueStatus {
        match self {
            ClueState::Available(_) => ClueStatus::Available,
            ClueState::Solved(_) => ClueStatus::Solved,
        }
    }
}
