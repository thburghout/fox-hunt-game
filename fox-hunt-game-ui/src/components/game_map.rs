use super::button::Button;
use leaflet::yew::{Map, Marker, Polyline};
use leaflet::{LatLng, TileLayer};
use load_dotenv::load_dotenv;
use yew::prelude::*;
use yew::services::ConsoleService;

load_dotenv!();

pub struct GameMap {
    props: Props,
    link: ComponentLink<Self>,
    selected_position: LatLng,
}

pub enum Msg {
    SetPos(LatLng),
    Submit,
    Reset,
}

#[derive(Properties, Clone, PartialEq)]
pub struct Props {
    pub disabled: bool,
    pub loading: bool,
    pub on_submit: Callback<LatLng>,
    pub path: Vec<LatLng>,
}

const START_POS: LatLng = LatLng {
    lat: 52.0,
    lng: 6.0,
};

impl Component for GameMap {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            props,
            link,
            selected_position: START_POS,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::SetPos(pos) => {
                self.selected_position = pos;
                true
            }
            Msg::Submit => {
                self.props.on_submit.emit(self.selected_position);
                false
            }
            Msg::Reset => {
                ConsoleService::log("Reset map");
                false
            }
        }
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        if self.props != props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        html!(
            <div class="card">
                <Map tile_layer=Self::tile_layer() center=LatLng::new(52.0, 6.0) zoom=13.0>
                    <Marker lat_lng=LatLng::new(52.0, 6.0) draggable=true
                            on_drag=self.link.callback(|e| { Msg::SetPos(e) })/>
                    <Polyline path=self.props.path.clone() color="red" />
                </Map>
                <div class="card-body clearfix">
                    <button class="btn btn-outline-secondary float-start"
                            disabled=self.props.disabled
                            onclick=self.link.callback(|_| { Msg::Reset })>
                        {"Reset"}
                    </button>
                    <div class="float-end">
                        <span class="text-muted d-inline-block align-middle me-2">
                            {format!("You selected {:.3}", self.selected_position)}
                        </span>
                        <Button disabled=self.props.disabled onclick=self.link.callback(|_| { Msg::Submit })
                            is_loading=self.props.loading >
                            {"Submit"}
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}

impl GameMap {
    fn tile_layer() -> TileLayer {
        TileLayer::new_with_options(
            "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}"
                .to_owned(),
            serde_json::json!({
                "attribution": "Map data &copy; <a href='https://www.openstreetmap.org/copyright'>OpenStreetMap</a> contributors, Imagery © <a href='https://www.mapbox.com/'>Mapbox</a>",
                "maxZoom": 18,
                "id": "mapbox/streets-v11",
                "tileSize": 512,
                "zoomOffset": -1,
                "accessToken": env!("MAPBOX_TOKEN"),
            }),
        )
    }
}
