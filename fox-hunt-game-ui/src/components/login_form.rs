use anyhow::{Error, Result};
use serde::{Deserialize, Serialize};
use yew::{
    format::{Json, Nothing},
    html::ChangeData::Select,
    prelude::{html, Component, ComponentLink, Html, ShouldRender},
    services::{
        fetch::{FetchTask, Request, Response},
        FetchService,
    },
    Callback, ChangeData, FocusEvent, Properties,
};

pub struct LoginForm {
    props: LoginFormProps,
    link: ComponentLink<Self>,
    fetch_task: Option<FetchTask>,
    teams: Teams,
    picked_team: i32,
    password: String,
    error: String,
    state: FormState,
}

#[derive(PartialEq)]
enum FormState {
    LoadingTeams,
    UserInput,
    LoggingIn,
}

#[derive(Serialize, Debug)]
pub struct LoginRequest {
    team_id: i32,
    password: String,
}

#[derive(Deserialize, Debug)]
pub struct LoginResponse {
    token: String,
}

#[derive(Deserialize)]
pub struct Team {
    pub id: i32,
    pub name: String,
}
type Teams = Vec<Team>;

impl Team {
    fn view(&self) -> Html {
        html!(<option value={self.id}>{self.name.clone()}</option>)
    }
}

impl LoginForm {
    fn view_select_team(&self) -> Html {
        if let FormState::LoadingTeams = self.state {
            html!(
                <div style={"margin: 6px;"} class="spinner-grow spinner-grow" role="status"></div>
            )
        } else {
            html!(
                <select onchange=self.link.callback(|x| Msg::Input(x)) class="form-control">
                    <option value="" disabled=true selected=true hidden=true>{"Select team"}</option>
                    { self.teams.iter().map(Team::view).collect::<Html>() }
                </select>
            )
        }
    }

    fn view_error(&self) -> Html {
        if !self.error.is_empty() {
            html!(<div class="alert alert-danger" role="alert">{ &self.error }</div>)
        } else {
            html!()
        }
    }

    fn login(&mut self) {
        assert!(self.fetch_task.is_none());
        let body = LoginRequest {
            team_id: self.picked_team,
            password: self.password.clone(),
        };
        let request = Request::post("/api/v1/login")
            .header("Content-Type", "application/json")
            .body(Json(&body))
            .expect("Could not build request.");
        let callback = self.link.callback(|response: Response<Result<String>>| {
            if response.status().is_success() {
                Msg::LoginResult(Ok(()))
            } else if response.status().is_client_error() {
                Msg::LoginResult(Result::Err(Error::msg("Incorrect credentials.")))
            } else {
                Msg::LoginResult(Result::Err(Error::msg("Server error :( ")))
            }
        });
        let task = FetchService::fetch(request, callback).expect("Failed to start request");
        self.fetch_task = Some(task);
    }
}

pub enum Msg {
    SetTeams(Teams),
    Input(ChangeData),
    Submit,
    LoginResult(Result<(), anyhow::Error>),
}

#[derive(Properties, Clone, PartialEq)]
pub struct LoginFormProps {
    pub on_login: Callback<()>,
    pub error: Option<String>,
}

impl Component for LoginForm {
    type Message = Msg;
    type Properties = LoginFormProps;
    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let request = Request::get("/api/v1/teams")
            .body(Nothing)
            .expect("Could not build request.");
        let callback = link.callback(|response: Response<Json<Result<Teams>>>| {
            let Json(data) = response.into_body();
            Msg::SetTeams(data.unwrap_or_default())
        });
        let task = FetchService::fetch(request, callback).expect("Failed to start request");
        let error = props.error.clone().take().unwrap_or_default();
        Self {
            props,
            link,
            fetch_task: Some(task),
            teams: vec![],
            picked_team: -1,
            password: "".to_string(),
            error,
            state: FormState::LoadingTeams,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::SetTeams(teams) => {
                self.fetch_task = None;
                self.teams = teams;
                self.state = FormState::UserInput;
            }
            Msg::Submit => {
                self.state = FormState::LoggingIn;
                self.error.clear();
                self.login();
            }
            Msg::Input(data) => match data {
                ChangeData::Value(pw) => {
                    self.password = pw;
                }
                Select(elem) => {
                    self.picked_team = elem.value().parse().unwrap();
                }
                _ => {
                    panic!("Invalid form data change");
                }
            },
            Msg::LoginResult(result) => {
                self.fetch_task = None;
                match result {
                    Ok(_) => {
                        self.props.on_login.emit(());
                    }
                    Err(error) => {
                        self.error = error.to_string();
                        self.state = FormState::UserInput;
                        self.password.clear();
                    }
                }
            }
        }
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        html! {
            <form class="form-signin" onsubmit=self.link.callback(|e: FocusEvent| {
                    e.prevent_default();
                    Msg::Submit})
                target="#">
                <img class="md-4" src="logo.png" width="100%"/>
                <h1 class="h3 mb-3 fw-normal">{"Team Login"}</h1>
                { self.view_error() }
                { self.view_select_team() }
                <input onchange=self.link.callback(|v| Msg::Input(v))
                    type="password" class="form-control"
                    autofocus=true placeholder="Password" value={&self.password}/>
                <button class="w-100 btn btn-lg btn-primary" type="submit" disabled={ FormState::LoggingIn == self.state}>
                    <span class={ if FormState::LoggingIn == self.state {"spinner-grow spinner-grow-sm"}
                                  else {""}}
                        role="status" aria-hidden="true"></span>
                    {"Sign in"}
                </button>
            </form>
        }
    }
}
