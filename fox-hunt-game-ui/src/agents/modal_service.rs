use yew::worker::{Agent, AgentLink, Context, HandlerId};
use yew::Dispatched;

pub struct ModalService {
    link: AgentLink<ModalService>,
    provider: Option<HandlerId>,
}

pub struct Modal {
    pub title: String,
    pub message: String,
}

pub struct ModalBuilder {
    title: String,
}

impl Modal {
    pub fn title(title: &str) -> ModalBuilder {
        ModalBuilder {
            title: String::from(title),
        }
    }
}

impl ModalBuilder {
    pub fn message(self, message: &str) -> Modal {
        Modal {
            title: self.title,
            message: String::from(message),
        }
    }
}

impl ModalService {
    pub fn show(message: Modal) {
        let mut service = ModalService::dispatcher();
        service.send(message);
    }
}

impl Agent for ModalService {
    type Reach = Context<Self>;
    type Message = ();
    type Input = Modal;
    type Output = Modal;

    fn create(link: AgentLink<Self>) -> Self {
        Self {
            link,
            provider: None,
        }
    }

    fn update(&mut self, _msg: Self::Message) {}

    fn connected(&mut self, id: HandlerId) {
        if id.is_respondable() {
            assert!(self.provider.is_none());
            self.provider = Some(id);
        }
    }

    fn handle_input(&mut self, msg: Self::Input, _: HandlerId) {
        if let Some(provider) = self.provider.as_ref() {
            self.link.respond(*provider, msg)
        }
    }

    fn disconnected(&mut self, id: HandlerId) {
        self.provider = self.provider.filter(|&provider| provider == id);
    }
}
