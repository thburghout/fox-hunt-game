import init, { run_app } from '../pkg/fox_hunt_game_ui.d.ts';
async function main() {
    await init('/pkg/fox_hunt_game_ui_bg.wasm');
    run_app();
}
main()
