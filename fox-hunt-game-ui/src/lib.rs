#![recursion_limit = "1024"]
#![allow(dead_code)]
mod agents;
mod components;

extern crate leaflet as L;

use crate::components::app::App as MyApp;
use std::panic;
use wasm_bindgen::prelude::wasm_bindgen;
use yew::prelude::App;

#[wasm_bindgen]
pub fn run_app() {
    panic::set_hook(Box::new(console_error_panic_hook::hook));

    App::<MyApp>::new().mount_to_body();
}
