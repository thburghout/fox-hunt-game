DROP TABLE IF EXISTS teams;
DROP TABLE IF EXISTS clues;
DROP TABLE IF EXISTS team_clue;

CREATE TABLE IF NOT EXISTS teams
(
    id       INTEGER PRIMARY KEY,
    name     TEXT NOT NULL,
    color    TEXT NOT NULL,
    password TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS clues
(
    id              INTEGER PRIMARY KEY,
    title           TEXT             NOT NULL,
    lat             REAL             NOT NULL,
    lng             REAL             NOT NULL,
    meters_accurate INTEGER UNSIGNED NOT NULL,
    content         TEXT             NOT NULL
);

CREATE TABLE IF NOT EXISTS team_clue
(
    team_id      INTEGER  NOT NULL,
    clue_id      INTEGER  NOT NULL,
    status       INTEGER  NOT NULL,
    published_at DATETIME NOT NULL,
    PRIMARY KEY (team_id, clue_id),
    CONSTRAINT fk_teams FOREIGN KEY (team_id) REFERENCES teams (id),
    CONSTRAINT fk_clues FOREIGN KEY (clue_id) REFERENCES clues (id)
);


CREATE TABLE IF NOT EXISTS hints
(
    clue_id     INTEGER NOT NULL,
    behind_by   INTEGER NOT NULL,
    content     TEXT    NOT NULL,
    PRIMARY KEY (clue_id, behind_by),
    CONSTRAINT fk_clues FOREIGN KEY (clue_id) REFERENCES clues (id)
);

