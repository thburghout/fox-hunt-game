use ::fox_hunt_game_backend::{server::server, util::env};
use clap::Clap;

#[derive(Clap)]
struct Opts {
    #[clap(short, long)]
    env: Option<String>,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let opts: Opts = Opts::parse();
    env::dotenv(opts.env.clone());
    server().await
}
