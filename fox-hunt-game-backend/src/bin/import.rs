use ::fox_hunt_game_backend::{
    models,
    models::{LatLng, Model, Team},
    util::env,
};
use anyhow::Result;
use clap::Clap;
use fox_hunt_game_backend::config::Config;

use fox_hunt_game_backend::models::TeamClue;
use fox_hunt_game_backend::util::Pool;
use futures::executor::block_on;
use serde::Deserialize;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;

#[derive(Clap)]
struct Opts {
    #[clap(short, long)]
    env: Option<String>,

    #[clap(short, long, default_value = "game.yml")]
    game_file: String,

    #[clap(long)]
    clean: bool,

    #[clap(long)]
    refresh_schema: bool,
}

#[derive(Debug, Deserialize)]
struct Game {
    teams: Vec<Team>,
    clues: Vec<Clue>,
}

#[derive(Debug, Deserialize)]
struct Clue {
    pub id: i64,
    pub title: String,
    pub answer: LatLng,
    pub meters_accurate: i64,
    pub content: String,
    pub hints: Option<Vec<Hint>>,
}

#[derive(Debug, Deserialize)]
struct Hint {
    pub behind_by: i64,
    pub content: String,
}

fn main() {
    block_on(async_main());
}

async fn async_main() {
    let opt: Opts = Opts::parse();
    env::dotenv(opt.env.clone());

    let config = envy::from_env::<Config>()
        .unwrap_or_else(|e| panic!("Error while loading environment variables: {}", e));
    let mut game = Game::from_file(opt.game_file.clone().as_ref())
        .unwrap_or_else(|e| panic!("Error while reading game file {}: {}", opt.game_file, e));

    let db = Pool::connect(&config.database_url)
        .await
        .unwrap_or_else(|e| panic!("Error connecting to db {}: {:?}", config.database_url, e));

    if opt.clean {
        println!("Removing data...");
        let delete_from = |t, db| async move {
            let _ = sqlx::query(&format!("DELETE FROM {}", t))
                .execute(db)
                .await
                .map_err(|e| println!("Error during DELETE: {}", e));
        };
        for table in ["team_clue", "hints", "clues", "teams"].iter() {
            delete_from(table.to_string(), &db).await;
        }
        if opt.refresh_schema {
            println!("Refreshing schema...");
            sqlx::query_file!("../schema.sql")
                .execute(&db)
                .await
                .unwrap();
        }
    }
    let hints: Vec<models::Hint> = game
        .clues
        .iter_mut()
        .map(Clue::take_hints)
        .flatten()
        .collect();
    let clues: Vec<models::Clue> = game.clues.into_iter().map(Into::into).collect();
    update("Teams", &db, &game.teams).await;
    update("Clues", &db, &clues).await;
    update("Hints", &db, &hints).await;

    if opt.clean {
        let teams = Team::all(&db).await.unwrap();
        let clue = models::Clue::first(&db).await.unwrap();
        for team in teams {
            TeamClue::publish(&db, &team, &clue).await.unwrap();
        }
    }
}

async fn update<M: Model + PartialEq>(name: &str, db: &Pool, models: &[M]) {
    let mut updated = 0;
    let mut inserted = 0;
    for model in models {
        let original = Model::find_by_id(db, model.id()).await;
        match original.ok() {
            Some(original) => {
                if model != &original {
                    model.update(db).await.unwrap();
                    updated += 1;
                }
            }
            None => {
                model.overwrite(db).await.unwrap();
                inserted += 1;
            }
        }
    }
    println!(
        "{}: updated {} rows, inserted {} new rows.",
        name, updated, inserted
    );
}

impl Game {
    pub fn from_file(path: &Path) -> Result<Game> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);
        let config: Game = serde_yaml::from_reader(reader)?;
        Ok(config)
    }
}

#[allow(clippy::from_over_into)]
impl Into<models::Clue> for Clue {
    fn into(self) -> models::Clue {
        models::Clue {
            id: self.id,
            title: self.title,
            answer: self.answer,
            meters_accurate: self.meters_accurate,
            content: self.content,
        }
    }
}

impl Clue {
    fn take_hints(&mut self) -> Vec<models::Hint> {
        self.hints.take().map_or(vec![], |hints| {
            hints
                .into_iter()
                .map(|h| models::Hint {
                    clue_id: self.id,
                    behind_by: h.behind_by,
                    content: h.content,
                })
                .collect()
        })
    }
}
