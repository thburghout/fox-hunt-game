use serde::{Deserialize, Serialize};
use std::path::PathBuf;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Config {
    pub app_key: String,
    pub env: Option<String>,
    pub database_url: String,
    pub host: Option<String>,
    pub port: Option<u16>,
}

impl Config {
    const PRODUCTION: &'static str = "production";

    pub fn production(&self) -> bool {
        self.env.as_ref().map_or(true, |x| x == Self::PRODUCTION)
    }

    pub fn port(&self) -> u16 {
        self.port.unwrap_or(80)
    }

    pub fn host(&self) -> &str {
        self.host.as_ref().map_or("0.0.0.0", |h| &h)
    }

    pub fn dist(&self) -> PathBuf {
        PathBuf::from(if self.production() {
            "/var/lib/fox-hunt-game/dist/"
        } else {
            "fox-hunt-game-ui/dist/"
        })
    }
}
