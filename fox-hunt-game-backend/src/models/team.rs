use actix_identity::Identity;

use actix_web::dev::Payload;
use actix_web::dev::PayloadStream;
use actix_web::error;
use actix_web::web::Data;
use actix_web::Error;
use actix_web::{FromRequest, HttpRequest};
use anyhow::Result;
use fox_hunt_game_msg as msg;

use futures_util::future::LocalBoxFuture;
use serde::{Deserialize, Serialize};

use crate::util::Pool;

use crate::models::model::Model;

use crate::models::TeamClue;
use async_trait::async_trait;

#[derive(Debug, Clone, Serialize, Deserialize, sqlx::FromRow, PartialEq)]
pub struct Team {
    pub id: i64,
    pub name: String,
    pub color: String,

    #[serde(skip_serializing)]
    pub password: String,
}

impl Team {
    pub async fn get_progress(&self, pool: &Pool) -> Result<f32> {
        TeamClue::get_progress_for_team(pool, &self).await
    }
}

#[async_trait]
impl Model for Team {
    type Id = i64;

    fn id(&self) -> i64 {
        self.id
    }

    async fn find_by_id(pool: &Pool, id: i64) -> Result<Team> {
        let team = sqlx::query_as!(Team, "SELECT * FROM teams WHERE id = ?", id)
            .fetch_one(&*pool)
            .await?;
        Ok(team)
    }

    async fn update(&self, pool: &Pool) -> Result<()> {
        sqlx::query!(
            "UPDATE teams SET \
                name = ?,
                color = ?,
                password = ?
            WHERE
                id = ?
        ",
            self.name,
            self.color,
            self.password,
            self.id
        )
        .execute(&*pool)
        .await?;
        Ok(())
    }

    async fn overwrite(&self, pool: &Pool) -> Result<()> {
        sqlx::query!(
            "INSERT INTO teams (id, name, color, password) VALUES (?, ?, ?, ?)",
            self.id,
            self.name,
            self.color,
            self.password
        )
        .execute(&*pool)
        .await?;
        Ok(())
    }

    async fn delete(self, pool: &Pool) -> Result<()> {
        sqlx::query!("DELETE FROM teams WHERE id = ?", self.id)
            .execute(&*pool)
            .await?;
        Ok(())
    }

    async fn all(pool: &Pool) -> Result<Vec<Team>> {
        Ok(sqlx::query_as!(Self, "SELECT * FROM teams")
            .fetch_all(&*pool)
            .await?)
    }
}

impl FromRequest for Team {
    type Error = Error;
    type Future = LocalBoxFuture<'static, Result<Self, Error>>;
    type Config = ();

    fn from_request(req: &HttpRequest, payload: &mut Payload<PayloadStream>) -> Self::Future {
        let id = Identity::from_request(req, payload)
            .into_inner()
            .expect("identity")
            .identity();
        let pool = req
            .app_data::<Data<Pool>>()
            .unwrap_or_else(|| panic!("Error while obtaining db pool"))
            .clone();
        let id = id.map(|x| x.parse::<i64>().ok()).flatten();
        match id {
            Some(id) => Box::pin(async move {
                Team::find_by_id(pool.get_ref(), id).await.map_err(|_| {
                    log::warn!("User authenticated with unknown id [{}]", id);
                    error::ErrorUnauthorized("Unauthorized")
                })
            }),
            None => Box::pin(async { Err(error::ErrorUnauthorized("Unauthorized2")) }),
        }
    }
}

#[allow(clippy::from_over_into)]
impl Into<msg::Team> for Team {
    fn into(self) -> msg::Team {
        msg::Team {
            id: self.id,
            name: self.name,
            color: self.color,
        }
    }
}
