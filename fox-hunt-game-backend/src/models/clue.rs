use crate::models::model::Model;

use crate::util::Pool;
use anyhow::Result;
use async_trait::async_trait;

use fox_hunt_game_msg::LatLng as LatLngMsg;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct Clue {
    pub id: i64,
    pub title: String,
    pub answer: LatLng,
    pub meters_accurate: i64,
    pub content: String,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq)]
pub struct LatLng {
    pub lat: f32,
    pub lng: f32,
}

impl Clue {
    pub async fn first(pool: &Pool) -> Result<Self> {
        sqlx::query_as!(
            ClueRow,
            "SELECT * FROM clues WHERE id IN (SELECT MIN(id) FROM clues)"
        )
        .fetch_one(&*pool)
        .await
        .map(From::from)
        .map_err(From::from)
    }
}

#[async_trait]
impl Model for Clue {
    type Id = i64;

    fn id(&self) -> Self::Id {
        self.id
    }

    async fn find_by_id(pool: &Pool, id: Self::Id) -> Result<Self> {
        sqlx::query_as!(ClueRow, "SELECT * FROM clues WHERE id = ?", id)
            .fetch_one(&*pool)
            .await
            .map(From::from)
            .map_err(From::from)
    }

    async fn update(&self, pool: &Pool) -> Result<()> {
        sqlx::query!(
            "UPDATE clues SET \
                title = ?, lat = ?, lng = ?, meters_accurate = ?, content = ?
            WHERE
                id = ?",
            self.title,
            self.answer.lat,
            self.answer.lng,
            self.meters_accurate,
            self.content,
            self.id
        )
        .execute(&*pool)
        .await
        .map_err(From::from)
        .map(|_| ())
    }

    async fn overwrite(&self, pool: &Pool) -> Result<()> {
        sqlx::query!(
            "INSERT INTO clues (id, title, lat, lng, meters_accurate, content) \
                VALUES (?, ?, ?, ?, ?, ?)",
            self.id,
            self.title,
            self.answer.lat,
            self.answer.lng,
            self.meters_accurate,
            self.content
        )
        .execute(&*pool)
        .await
        .map_err(From::from)
        .map(|_| ())
    }

    async fn delete(self, pool: &Pool) -> Result<()> {
        sqlx::query!("DELETE FROM clues WHERE id = ?", self.id)
            .execute(&*pool)
            .await?;
        Ok(())
    }

    async fn all(pool: &Pool) -> Result<Vec<Self>> {
        sqlx::query_as!(ClueRow, "SELECT * FROM clues")
            .fetch_all(&*pool)
            .await
            .map_err(From::from)
            .map(|xs| xs.into_iter().map(From::from).collect())
    }
}

#[derive(sqlx::FromRow)]
struct ClueRow {
    id: i64,
    title: String,
    lat: f32,
    lng: f32,
    meters_accurate: i64,
    content: String,
}

impl From<ClueRow> for Clue {
    fn from(r: ClueRow) -> Self {
        Clue {
            id: r.id,
            title: r.title,
            answer: LatLng {
                lat: r.lat,
                lng: r.lng,
            },
            meters_accurate: r.meters_accurate,
            content: r.content,
        }
    }
}

impl LatLng {
    pub fn new(lat: f32, lng: f32) -> Self {
        LatLng { lat, lng }
    }
}

#[allow(clippy::from_over_into)]
impl Into<LatLngMsg> for LatLng {
    fn into(self) -> LatLngMsg {
        LatLngMsg {
            lat: self.lat,
            lng: self.lng,
        }
    }
}
