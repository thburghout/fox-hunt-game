use crate::models::Model;
use crate::util::Pool;
use anyhow::Result;
use async_trait::async_trait;

#[derive(Debug, PartialEq)]
pub struct Hint {
    pub clue_id: i64,
    pub behind_by: i64,
    pub content: String,
}

#[async_trait]
impl Model for Hint {
    type Id = (i64, i64);

    fn id(&self) -> Self::Id {
        (self.clue_id, self.behind_by)
    }

    async fn find_by_id(pool: &Pool, id: Self::Id) -> Result<Self> {
        let (clue_id, behind_by) = id;
        sqlx::query_as!(
            Hint,
            "SELECT * FROM hints WHERE clue_id = ? AND behind_by = ?",
            clue_id,
            behind_by
        )
        .fetch_one(&*pool)
        .await
        .map_err(From::from)
        .map(From::from)
    }

    async fn update(&self, pool: &Pool) -> Result<()> {
        sqlx::query!(
            "UPDATE hints SET content = ? WHERE clue_id = ? AND behind_by = ?",
            self.content,
            self.clue_id,
            self.behind_by
        )
        .execute(&*pool)
        .await?;
        Ok(())
    }

    async fn overwrite(&self, pool: &Pool) -> Result<()> {
        sqlx::query!(
            "INSERT INTO hints (clue_id, behind_by, content) VALUES (?, ?, ?)",
            self.clue_id,
            self.behind_by,
            self.content
        )
        .execute(&*pool)
        .await?;
        Ok(())
    }

    async fn delete(self, pool: &Pool) -> Result<()> {
        sqlx::query!(
            "DELETE FROM hints WHERE clue_id = ? AND behind_by = ?",
            self.clue_id,
            self.behind_by
        )
        .execute(&*pool)
        .await?;
        Ok(())
    }

    async fn all(pool: &Pool) -> Result<Vec<Self>> {
        sqlx::query_as!(Hint, "SELECT * FROM hints")
            .fetch_all(&*pool)
            .await
            .map_err(From::from)
            .map(|xs| xs.into_iter().map(From::from).collect())
    }
}
