use crate::models::{Clue, LatLng, Team};
use crate::util::Pool;
use anyhow::anyhow;
use anyhow::Result;
use chrono::{DateTime, Utc};
use fox_hunt_game_msg::{
    AvailableClueProps as AvailableClueMsg, Clue as ClueMsg, ClueState as ClueStateMsg,
    SolvedClueProps as SolvedClueMsg,
};
use futures::future::join_all;
use sqlx::Error;

#[derive(Debug, Clone)]
pub struct TeamClue {
    pub id: i64,
    pub title: String,
    pub content: String,
    pub published_at: DateTime<Utc>,
    pub state: ClueState,
    pub team: Team,
}

#[derive(Debug, Clone)]
pub struct SolvedClueProps {
    pub location: LatLng,
}

#[derive(Debug, Clone)]
pub struct AvailableClueProps {
    pub hints: Vec<String>,
}

#[derive(Debug, Clone)]
pub enum ClueState {
    Available(AvailableClueProps),
    Solved(SolvedClueProps),
}

#[derive(sqlx::Type, Debug, Copy, Clone)]
#[repr(i64)]
enum ClueStatus {
    Available,
    Solved,
}

#[derive(sqlx::Type, Debug, Clone)]
struct TeamClueRow {
    pub id: i64,
    pub title: String,
    pub status: ClueStatus,
    pub content: String,
    pub published_at: DateTime<Utc>,
    pub lat: f32,
    pub lng: f32,
}

impl TeamClue {
    pub async fn get_for_team(pool: &Pool, team: &Team) -> Result<Vec<TeamClue>> {
        let result: Vec<TeamClueRow> = sqlx::query_as!(
            TeamClueRow,
            r#"
                SELECT clues.id as id, title, status as "status: ClueStatus", content, published_at as "published_at: _", lat, lng
                FROM clues
                INNER JOIN team_clue ON clues.id = team_clue.clue_id
                WHERE team_clue.team_id = ?"#,
            team.id
        ).fetch_all(&*pool).await?;
        let hints_of_teams = result
            .iter()
            .map(|x| Self::hints_for_team(team, x.id, pool));
        let hints_of_teams: Vec<Vec<String>> = join_all(hints_of_teams)
            .await
            .into_iter()
            .collect::<Result<_>>()?;
        Ok(result
            .into_iter()
            .zip(hints_of_teams)
            .map(|(row, hints)| row.into_team_clue(team.clone(), hints))
            .collect())
    }

    pub async fn find_for(pool: &Pool, team: &Team, clue: &Clue) -> Result<TeamClue> {
        let hints = Self::hints_for_team(team, clue.id, pool).await?;
        Ok(sqlx::query_as!(
            TeamClueRow,
            r#"
                SELECT clues.id as id, title, status as "status: ClueStatus", content, published_at as "published_at: _", lat, lng
                FROM clues
                INNER JOIN team_clue ON clues.id = team_clue.clue_id
                WHERE team_clue.team_id = ? AND clues.id = ?"#,
            team.id, clue.id
        )
            .fetch_one(&*pool)
            .await?
            .into_team_clue(team.clone(), hints))
    }

    pub async fn find_current_available_clue(pool: &Pool, team: &Team) -> Result<Option<TeamClue>> {
        let result = sqlx::query_as!(TeamClueRow, r#"
            SELECT clues.id as id, title, status as "status: ClueStatus", content, published_at as "published_at: _", lat, lng
            FROM clues
            INNER JOIN team_clue ON clues.id = team_clue.clue_id
            WHERE team_clue.team_id = ? AND team_clue.status = ?
            ORDER BY published_at DESC"#, team.id, ClueStatus::Available)
            .fetch_one(&*pool).await;
        match result {
            Ok(row) => {
                let hints = Self::hints_for_team(team, row.id, pool).await?;
                Ok(Some(row.into_team_clue(team.clone(), hints)))
            }
            Err(Error::RowNotFound) => Ok(None),
            Err(x) => Err(x.into()),
        }
    }

    pub async fn solve(pool: &Pool, team: &Team, clue: &Clue) -> Result<()> {
        sqlx::query!(
            "UPDATE team_clue SET status = ? WHERE team_id = ? AND clue_id = ?",
            ClueStatus::Solved,
            team.id,
            clue.id,
        )
        .execute(&*pool)
        .await?;
        Ok(())
    }

    pub async fn publish(pool: &Pool, team: &Team, clue: &Clue) -> Result<()> {
        let now = Utc::now();
        sqlx::query!(
            "INSERT INTO team_clue (team_id, clue_id, status, published_at) \
                      VALUES (?, ?, ?, ?)",
            team.id,
            clue.id,
            ClueStatus::Available,
            now
        )
        .execute(&*pool)
        .await?;
        Ok(())
    }

    pub async fn get_progress_for_all(pool: &Pool) -> Result<Vec<(Team, f32)>> {
        let result = sqlx::query!(
            "
            SELECT id, name, color, password, 1.0/total*solved AS progress
            FROM (
                SELECT teams.*, total, COUNT(clue_id) AS solved
                FROM teams
                LEFT JOIN (SELECT COUNT(*) as total FROM clues)
                LEFT JOIN (SELECT * FROM team_clue WHERE status IS ?) tc
                    ON teams.id = tc.team_id
                GROUP BY id, name, password, total)",
            ClueStatus::Solved
        )
        .fetch_all(&*pool)
        .await?;
        Ok(result
            .into_iter()
            .map(|x| {
                (
                    Team {
                        id: x.id.unwrap(),
                        name: x.name,
                        color: x.color,
                        password: x.password,
                    },
                    x.progress.unwrap(),
                )
            })
            .collect())
    }

    pub async fn get_solved_count_for_all(pool: &Pool) -> Result<Vec<(i64, i32)>> {
        let counts = sqlx::query!(
            "
            SELECT teams.id as team_id, COUNT(clue_id) as 'solved_clue_count: i32'
            FROM teams
            LEFT JOIN (
                SELECT * FROM team_clue WHERE status IS ?
            ) tc ON teams.id = tc.team_id
            GROUP BY id;",
            ClueStatus::Solved
        )
        .fetch_all(&*pool)
        .await?;
        Ok(counts
            .into_iter()
            .map(|x| (x.team_id.unwrap(), x.solved_clue_count.unwrap()))
            .collect())
    }

    pub async fn get_progress_for_team(pool: &Pool, team: &Team) -> Result<f32> {
        let result = sqlx::query!(
            r#"
            SELECT 1.0/total*solved as "progress: f32"
            FROM (
                SELECT COUNT(*) as solved
                FROM team_clue
                WHERE team_id = ? AND status = ?)
            LEFT JOIN (SELECT COUNT(*) as total FROM clues)"#,
            team.id,
            ClueStatus::Solved
        )
        .fetch_one(&*pool)
        .await?;
        result
            .progress
            .map_or(Err(anyhow!("No progress for team.id: {}", team.id)), Ok)
    }

    async fn difference_to_best_team(t: &Team, pool: &Pool) -> Result<i32> {
        let counts = Self::get_solved_count_for_all(pool).await?;
        let top = counts
            .iter()
            .max_by_key(|(_, count)| count)
            .expect("all teams should have scores");
        let my_count = counts
            .iter()
            .find(|(team_id, _)| *team_id == t.id)
            .expect("Own team should exists");
        let diff_to_top = top.1 - my_count.1;
        Ok(if diff_to_top < 0 {
            log::warn!("Team score diff to max(score) is < 0?");
            0
        } else {
            diff_to_top
        })
    }

    async fn hints_for_team(t: &Team, clue_id: i64, pool: &Pool) -> Result<Vec<String>> {
        let diff = Self::difference_to_best_team(t, pool).await?;
        Ok(sqlx::query!(
            "SELECT content FROM hints WHERE behind_by <= ? AND clue_id = ? ORDER BY behind_by",
            diff,
            clue_id
        )
        .fetch_all(&*pool)
        .await?
        .into_iter()
        .map(|x| x.content)
        .collect())
    }
}

impl TeamClueRow {
    fn into_team_clue(self, team: Team, hints: Vec<String>) -> TeamClue {
        TeamClue {
            id: self.id,
            title: self.title,
            content: self.content,
            published_at: self.published_at,
            state: match self.status {
                ClueStatus::Available => ClueState::Available(AvailableClueProps { hints }),
                ClueStatus::Solved => ClueState::Solved(SolvedClueProps {
                    location: LatLng::new(self.lat, self.lng),
                }),
            },
            team,
        }
    }
}

#[allow(clippy::from_over_into)]
impl Into<ClueMsg> for TeamClue {
    fn into(self) -> ClueMsg {
        ClueMsg::new(
            self.id,
            self.title,
            self.content,
            self.published_at,
            self.state.into(),
        )
    }
}

#[allow(clippy::from_over_into)]
impl Into<ClueStateMsg> for ClueState {
    fn into(self) -> ClueStateMsg {
        match self {
            ClueState::Available(x) => ClueStateMsg::Available(x.into()),
            ClueState::Solved(x) => ClueStateMsg::Solved(x.into()),
        }
    }
}

#[allow(clippy::from_over_into)]
impl Into<SolvedClueMsg> for SolvedClueProps {
    fn into(self) -> SolvedClueMsg {
        SolvedClueMsg {
            location: self.location.into(),
        }
    }
}

#[allow(clippy::from_over_into)]
impl Into<AvailableClueMsg> for AvailableClueProps {
    fn into(self) -> AvailableClueMsg {
        AvailableClueMsg { hints: self.hints }
    }
}
