use crate::util::Pool;
use anyhow::Result;
use async_trait::async_trait;

#[async_trait]
pub trait Model
where
    Self: Sized,
{
    type Id;

    fn id(&self) -> Self::Id;

    async fn find_by_id(pool: &Pool, id: Self::Id) -> Result<Self>;

    async fn update(&self, pool: &Pool) -> Result<()>;

    async fn overwrite(&self, pool: &Pool) -> Result<()>;

    async fn delete(self, pool: &Pool) -> Result<()>;

    async fn all(pool: &Pool) -> Result<Vec<Self>>;
}
