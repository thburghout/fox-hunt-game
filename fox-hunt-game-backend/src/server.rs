use crate::config::Config;
use crate::events::{Event, GameEvent};
use crate::handlers::teams::TeamActor;
use crate::models::{Model, Team};
use crate::routes::routes;
use actix_identity::{CookieIdentityPolicy, IdentityService};
use actix_web::middleware::Logger;
use actix_web::{App, HttpServer};
use sqlx::sqlite::SqlitePool;
use tokio::sync::broadcast;
use tokio::sync::broadcast::Sender;

pub async fn server() -> std::io::Result<()> {
    let config = envy::from_env::<Config>()
        .unwrap_or_else(|e| panic!("Error while loading environment variables: {}", e));
    println!("Config: {:?}", config);

    std::env::set_var("RUST_LOG", "info,actix_web=debug,sqlx=warn");
    env_logger::init();

    let pool = SqlitePool::connect(&config.database_url)
        .await
        .unwrap_or_else(|e| panic!("Error connecting to db {}: {:?}", config.database_url, e));

    let (ws_tx, _): (Sender<Event>, _) = broadcast::channel(16);
    let (game_events, _): (Sender<GameEvent>, _) = broadcast::channel(16);

    for team in Team::all(&pool).await.unwrap().into_iter() {
        TeamActor::start(pool.clone(), team, ws_tx.clone(), game_events.subscribe());
    }

    let addr = format!("{}:{}", config.host(), config.port());
    let create_app = move || {
        App::new()
            .wrap(Logger::default())
            .wrap(IdentityService::new(
                CookieIdentityPolicy::new(config.app_key.as_bytes())
                    .name("auth")
                    .secure(config.production()),
            ))
            .data(pool.clone())
            .data(ws_tx.clone())
            .data(game_events.clone())
            .data(config.clone())
            .configure(|s| routes(&config, s))
    };
    HttpServer::new(create_app).bind(&addr)?.run().await
}
