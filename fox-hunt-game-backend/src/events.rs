use crate::models::{Clue, Team, TeamClue};

#[derive(Clone, Debug)]
pub enum Event {
    ClueUpdate(Team, TeamClue),
    TeamProgress(Team, f32),
}

#[derive(Clone, Debug)]
pub enum GameEvent {
    TeamSolvedClue(Team, Clue),
}
