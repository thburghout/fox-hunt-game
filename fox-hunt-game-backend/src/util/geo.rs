extern crate geoutils;

use crate::models::LatLng;
use geoutils::Location;

impl From<LatLng> for Location {
    fn from(x: LatLng) -> Self {
        Location::new(x.lat, x.lng)
    }
}

pub(crate) fn distance(x: &LatLng, y: &LatLng) -> f64 {
    let x: Location = x.clone().into();
    let y: Location = y.clone().into();
    x.haversine_distance_to(&y).meters()
}
