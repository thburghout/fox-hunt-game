use dotenv::dotenv as dotenv_default;
use dotenv::from_path;

pub fn dotenv(path: Option<String>) {
    let _ = match path {
        Some(path) => from_path(path + ".env"),
        None => dotenv_default().map(|_| ()),
    };
}
