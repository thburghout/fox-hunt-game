use crate::events::GameEvent;
use crate::models::Clue;
use crate::models::{LatLng, Model, Team, TeamClue};
use crate::util::geo::distance;
use crate::util::Pool;
use actix_web::{post, web, HttpResponse, Result as WebResult};
use anyhow::Result;
use fox_hunt_game_msg::ClueId;
use serde_json::json;
use tokio::sync::broadcast::Sender;

#[post("/clue/{clue_id}/solve")]
pub async fn solve(
    team: Team,
    pool: web::Data<Pool>,
    clue_id: web::Path<ClueId>,
    position: web::Json<LatLng>,
    events: web::Data<Sender<GameEvent>>,
) -> WebResult<HttpResponse> {
    Ok(solve_clue(&*events, &pool, &team, *clue_id, *position)
        .await
        .map(|correct| {
            if correct {
                HttpResponse::Ok().json(json!({"result": "ok"}))
            } else {
                HttpResponse::Ok().json(json!({"result": "incorrect"}))
            }
        })
        .map_err(|e| {
            log::error!("{}", e);
            HttpResponse::InternalServerError().finish()
        })?)
}

async fn solve_clue(
    events: &Sender<GameEvent>,
    pool: &Pool,
    team: &Team,
    clue_id: ClueId,
    position: LatLng,
) -> Result<bool> {
    let clue = Clue::find_by_id(&pool, clue_id).await?;
    let meters_distance = distance(&clue.answer, &position);
    if meters_distance <= clue.meters_accurate as f64 {
        TeamClue::solve(&pool, team, &clue).await?;
        events
            .send(GameEvent::TeamSolvedClue(team.clone(), clue))
            .unwrap();
        Ok(true)
    } else {
        Ok(false)
    }
}
