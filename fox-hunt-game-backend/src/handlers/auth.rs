use crate::models::Team;

use crate::models::Model;
use crate::util::Pool;
use actix_identity::Identity;
use actix_web::{web, HttpResponse};
use fox_hunt_game_msg as msg;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct LoginResult {
    token: String,
}

#[derive(Serialize, Deserialize)]
pub struct LoginAttempt {
    team_id: i64,
    password: String,
}

pub async fn login(
    id: Identity,
    pool: web::Data<Pool>,
    request: web::Json<LoginAttempt>,
) -> HttpResponse {
    if let Ok(team) = Team::find_by_id(pool.as_ref(), request.team_id).await {
        if team.password == request.password {
            id.remember(team.id.to_string());
            HttpResponse::Ok().finish()
        } else {
            HttpResponse::Unauthorized().finish()
        }
    } else {
        HttpResponse::BadRequest().finish()
    }
}

pub async fn logout(id: Identity, _team: Team) -> HttpResponse {
    id.forget();
    HttpResponse::Ok().finish()
}

pub async fn me(team: Team) -> HttpResponse {
    let me: msg::Team = team.into();
    HttpResponse::Ok().json(me)
}
