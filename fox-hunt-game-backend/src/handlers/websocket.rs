use crate::events::Event;
use crate::models::{Team, TeamClue};
use crate::util::Pool;
use actix::{Actor, ActorFuture, ContextFutureSpawner, StreamHandler};
use actix::{AsyncContext, WrapFuture};
use actix_web::{web, Error, HttpRequest, HttpResponse};
use actix_web_actors::ws;
use fox_hunt_game_msg as msg;
use tokio::sync::broadcast::{Receiver, Sender};
use tokio_stream::wrappers::errors::BroadcastStreamRecvError;
use tokio_stream::wrappers::BroadcastStream;

struct WsContext {
    pool: Pool,
    team: Team,
    events: Option<Receiver<Event>>,
}

impl WsContext {
    pub fn new(pool: Pool, team: Team, events: Receiver<Event>) -> Self {
        WsContext {
            pool,
            team,
            events: Some(events),
        }
    }
}

impl Actor for WsContext {
    type Context = ws::WebsocketContext<Self>;
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for WsContext {
    fn handle(&mut self, msg: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        match msg {
            Ok(ws::Message::Ping(msg)) => ctx.pong(&msg),
            Ok(ws::Message::Text(text)) => ctx.text(text),
            Ok(ws::Message::Binary(bin)) => ctx.binary(bin),
            _ => (),
        }
    }

    fn started(&mut self, ctx: &mut Self::Context) {
        self.collect_and_transmit_clues(ctx);
        self.transmit_team_progress(ctx);
        ctx.add_stream(BroadcastStream::new(self.events.take().unwrap()));
    }
}

impl StreamHandler<Result<Event, BroadcastStreamRecvError>> for WsContext {
    fn handle(&mut self, item: Result<Event, BroadcastStreamRecvError>, ctx: &mut Self::Context) {
        match item {
            Ok(Event::ClueUpdate(team, event)) => {
                if team == self.team {
                    ctx.text(
                        serde_json::to_string(&msg::Message::ClueUpdate(event.into())).unwrap(),
                    )
                }
            }
            Ok(Event::TeamProgress(team, progress)) => {
                ctx.text(
                    serde_json::to_string(&msg::Message::TeamProgress(team.into(), progress))
                        .unwrap(),
                );
            }
            Err(error) => {
                log::error!("{:?}", error)
            }
        }
    }
}

impl WsContext {
    fn collect_and_transmit_clues(&self, ctx: &mut <WsContext as Actor>::Context) {
        let pool = self.pool.clone();
        let team = self.team.clone();
        async move {
            TeamClue::get_for_team(&pool, &team)
                .await
                .map_err(|e| log::warn!("Error occurred fetching clues {}", e))
                .map(|xs| xs.into_iter().map(Into::into).collect::<Vec<msg::Clue>>())
        }
        .into_actor(self)
        .map(|clues, _act, ctx| {
            if let Ok(clues) = clues {
                ctx.text(serde_json::to_string(&msg::Message::Clues(clues)).unwrap())
            }
        })
        .spawn(ctx);
    }

    fn transmit_team_progress(&mut self, ctx: &mut <WsContext as Actor>::Context) {
        let pool = self.pool.clone();
        async move { TeamClue::get_progress_for_all(&pool).await }
            .into_actor(self)
            .map(|result, _, ctx| {
                let result = result.map(|x| -> anyhow::Result<Vec<String>> {
                    x.into_iter()
                        .map(|(t, p)| {
                            serde_json::to_string(&msg::Message::TeamProgress(t.into(), p))
                                .map_err(Into::into)
                        })
                        .collect()
                });
                match result {
                    Ok(Ok(msgs)) => msgs.into_iter().for_each(|msg| ctx.text(msg)),
                    Err(e) | Ok(Err(e)) => log::warn!("Error during fetching team progress: {}", e),
                }
            })
            .spawn(ctx);
    }
}

pub async fn connect(
    team: Team,
    pool: web::Data<Pool>,
    req: HttpRequest,
    stream: web::Payload,
    events: web::Data<Sender<Event>>,
) -> Result<HttpResponse, Error> {
    ws::start(
        WsContext::new(pool.get_ref().clone(), team, events.get_ref().subscribe()),
        &req,
        stream,
    )
}
