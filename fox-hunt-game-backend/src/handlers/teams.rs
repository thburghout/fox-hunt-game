use crate::events::{Event, GameEvent};
use crate::models::{Clue, Model, Team, TeamClue};
use crate::util::Pool;
use actix::{
    Actor, ActorFuture, Addr, AsyncContext, Context, ContextFutureSpawner, StreamHandler,
    WrapFuture,
};
use actix_web::{web, HttpResponse};
use anyhow::Result;
use tokio::sync::broadcast::{Receiver, Sender};
use tokio_stream::wrappers::errors::BroadcastStreamRecvError;
use tokio_stream::wrappers::BroadcastStream;

pub async fn get_teams(pool: web::Data<Pool>) -> HttpResponse {
    let teams = Team::all(pool.as_ref()).await;
    match teams {
        Ok(teams) => HttpResponse::Ok().json(teams),
        Err(e) => {
            log::warn!("Error retrieving teams: {}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}

#[derive(Clone)]
pub struct TeamActor {
    pool: Pool,
    team: Team,
    websockets: Sender<Event>,
}

impl Actor for TeamActor {
    type Context = Context<Self>;
}

impl StreamHandler<Result<GameEvent, BroadcastStreamRecvError>> for TeamActor {
    fn handle(
        &mut self,
        item: Result<GameEvent, BroadcastStreamRecvError>,
        ctx: &mut Context<Self>,
    ) {
        match item {
            Ok(GameEvent::TeamSolvedClue(t, c)) => {
                self.clone()
                    .handle_team_solved_clue(t, c)
                    .into_actor(self)
                    .map(|x, _, _| {
                        let _ =
                            x.map_err(|e| log::error!("Error in solved team clue handler: {}", e));
                    })
                    .spawn(ctx);
            }
            Err(e) => {
                log::error!("Error in team actor stream handler: {:?}", e);
            }
        }
    }
}

impl TeamActor {
    pub fn start(
        pool: Pool,
        team: Team,
        websockets: Sender<Event>,
        receiver: Receiver<GameEvent>,
    ) -> Addr<TeamActor> {
        TeamActor::create(|c| {
            c.add_stream(BroadcastStream::new(receiver));
            TeamActor {
                pool,
                team,
                websockets,
            }
        })
    }

    async fn handle_team_solved_clue(self, team: Team, solved_clue: Clue) -> Result<()> {
        if team.id == self.team.id {
            self.handle_self_solved_clue(&solved_clue).await?;
        } else {
            self.handle_other_team_solved_clue(&team).await?;
        }
        self.publish_team_score(&team).await
    }

    async fn handle_self_solved_clue(&self, solved_clue: &Clue) -> Result<()> {
        let team_clue = TeamClue::find_for(&self.pool, &self.team, &solved_clue).await?;
        self.websockets
            .send(Event::ClueUpdate(self.team.clone(), team_clue))
            .unwrap();
        self.try_publish_next_clue(&solved_clue).await
    }

    async fn try_publish_next_clue(&self, previous_clue: &Clue) -> Result<()> {
        if let Ok(next_clue) = Clue::find_by_id(&self.pool, previous_clue.id + 1).await {
            TeamClue::publish(&self.pool, &self.team, &next_clue).await?;
            self.websockets
                .send(Event::ClueUpdate(
                    self.team.clone(),
                    TeamClue::find_for(&self.pool, &self.team, &next_clue).await?,
                ))
                .unwrap();
        }
        Ok(())
    }

    async fn handle_other_team_solved_clue(&self, _team: &Team) -> Result<()> {
        let team_clue = TeamClue::find_current_available_clue(&self.pool, &self.team).await?;
        if let Some(team_clue) = team_clue {
            self.websockets
                .send(Event::ClueUpdate(self.team.clone(), team_clue))
                .unwrap();
        }
        Ok(())
    }

    async fn publish_team_score(&self, team: &Team) -> Result<()> {
        self.websockets
            .send(Event::TeamProgress(
                team.clone(),
                team.get_progress(&self.pool).await?,
            ))
            .unwrap();
        Ok(())
    }
}
