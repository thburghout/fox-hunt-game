use actix_files as fs;
use actix_web::web;

use crate::config::Config;
use crate::handlers::auth;
use crate::handlers::clue;
use crate::handlers::teams::get_teams;
use crate::handlers::websocket::connect;
use actix_files::NamedFile;
use actix_web::Result;

pub fn routes(config: &Config, service: &mut web::ServiceConfig) {
    service
        .route("/", web::get().to(show_index))
        .service(
            web::scope("/api/v1")
                .route("/login", web::post().to(auth::login))
                .route("/logout", web::post().to(auth::logout))
                .route("/me", web::get().to(auth::me))
                .route("/teams", web::get().to(get_teams))
                .service(clue::solve),
        )
        .route("/ws", web::get().to(connect))
        .service(fs::Files::new("/", config.dist()));
}

pub async fn show_index(config: web::Data<Config>) -> Result<NamedFile> {
    Ok(NamedFile::open(config.dist().join("index.html"))?)
}
